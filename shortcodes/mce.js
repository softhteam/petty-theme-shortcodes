(function() {
	tinymce.PluginManager.add('petty_mce_button', function( editor, url ) {
		editor.addButton( 'petty_mce_button', {
            text: pettyLang.shortcode_petty, 
            icon: false,
			tooltip: pettyLang.shortcode_petty,
			type: 'menubutton',
			minWidth: 210,
			menu: [
				{
					text: pettyLang.shortcode_button,
					onclick: function() {
						editor.windowManager.open( {
							title: pettyLang.shortcode_button,
							body: [
								{
									type: 'listbox',
									name: 'ButtonType',
									label: pettyLang.shortcode_type,
									'values': [
										{text: pettyLang.shortcode_default, value: 'btn-default'},
										{text: pettyLang.shortcode_primary, value: 'btn-primary'},
										{text: pettyLang.shortcode_success, value: 'btn-success'},
										{text: pettyLang.shortcode_info, value: 'btn-info'},
										{text: pettyLang.shortcode_warning, value: 'btn-warning'},
										{text: pettyLang.shortcode_danger, value: 'btn-danger'},
									]
								},
								{
									type: 'listbox',
									name: 'ButtonSize',
									label: pettyLang.shortcode_size,
									'values': [
										{text: pettyLang.shortcode_size_large, value: 'btn-lg'},
										{text: pettyLang.shortcode_size_default, value: ''},
										{text: pettyLang.shortcode_size_small, value: 'btn-sm'},
										{text: pettyLang.shortcode_size_ex_small, value: 'btn-xs'},
									]
								},
								{
									type: 'textbox',
									name: 'ButtonText',
									label: pettyLang.shortcode_text,
									minWidth: 300,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[button type="' + e.data.ButtonType + '" size="' + e.data.ButtonSize + '" text="' + e.data.ButtonText + '"]');
							}
						});
					} // onclick
				},
				{
					text: pettyLang.shortcode_progressbar,
					onclick: function() {
						editor.windowManager.open( {
							title: pettyLang.shortcode_progressbar,
							body: [
								{
									type: 'listbox',
									name: 'ProgressbarType',
									label: pettyLang.shortcode_type,
									'values': [
										{text: pettyLang.shortcode_default, value: 'default'},
										{text: pettyLang.shortcode_primary, value: 'primary'},
										{text: pettyLang.shortcode_success, value: 'success'},
										{text: pettyLang.shortcode_info, value: 'info'},
										{text: pettyLang.shortcode_warning, value: 'warning'},
										{text: pettyLang.shortcode_danger, value: 'danger'},
									]
								},
								{
									type: 'textbox',
									name: 'ProgressbarWidth',
									label: pettyLang.shortcode_width,
									minWidth: 300,
									value: '50'
								},
								{
									type: 'textbox',
									name: 'ProgressbarTitle',
									label: pettyLang.shortcode_title,
									minWidth: 300,
									value: ''
								},
								{
									type: 'listbox',
									name: 'ProgressbarStriped',
									label: pettyLang.shortcode_striped,
									'values': [
										{text: pettyLang.shortcode_true, value: 'true'},
										{text: pettyLang.shortcode_false, value: 'false'},
									]
								},
								{
									type: 'listbox',
									name: 'ProgressbarAnimation',
									label: pettyLang.shortcode_animation,
									'values': [
										{text: pettyLang.shortcode_true, value: 'true'},
										{text: pettyLang.shortcode_false, value: 'false'},
									]
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[progress_bar type="' + e.data.ProgressbarType + '" width="' + e.data.ProgressbarWidth + '" title="' + e.data.ProgressbarTitle + '" striped="' + e.data.ProgressbarStriped + '" animation="' + e.data.ProgressbarAnimation + '"]');
							}
						});
					}
				},
				{
					text: pettyLang.shortcode_status,
					onclick: function() {
						editor.windowManager.open( {
							title: pettyLang.shortcode_status,
							body: [
								{
									type: 'listbox',
									name: 'StatusType',
									label: pettyLang.shortcode_type,
									'values': [
										{text: pettyLang.shortcode_facebook, value: 'facebook'},
										{text: pettyLang.shortcode_twitter, value: 'twitter'},
										{text: pettyLang.shortcode_google_plus, value: 'google_plus'},
									]
								},
								{
									type: 'textbox',
									name: 'StatusURL',
									label: pettyLang.shortcode_url,
									minWidth: 300,
									value: ''
								},
								{
									type: 'textbox',
									name: 'StatusBackground',
									label: pettyLang.shortcode_background_img,
									minWidth: 300,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[status type="' + e.data.StatusType + '" url="' + e.data.StatusURL + '" background="' + e.data.StatusBackground + '"]');
							}
						});
					}
				},
				{
					text: pettyLang.shortcode_alert,
					onclick: function() {
						editor.windowManager.open( {
							title: pettyLang.shortcode_alert,
							body: [
								{
									type: 'listbox',
									name: 'AlertType',
									label: pettyLang.shortcode_type,
									'values': [
										{text: pettyLang.shortcode_default, value: 'default'},
										{text: pettyLang.shortcode_primary, value: 'primary'},
										{text: pettyLang.shortcode_success, value: 'success'},
										{text: pettyLang.shortcode_info, value: 'info'},
										{text: pettyLang.shortcode_warning, value: 'warning'},
										{text: pettyLang.shortcode_danger, value: 'danger'},
									]
								},
								{
									type: 'listbox',
									name: 'AlertDismiss',
									label: pettyLang.shortcode_dismiss,
									'values': [
										{text: pettyLang.shortcode_true, value: 'true'},
										{text: pettyLang.shortcode_false, value: 'false'},
									]
								},
								{
									type: 'textbox',
									name: 'AlertContent',
									label: pettyLang.shortcode_content,
									value: '',									
									multiline: true,
									minWidth: 300,
									minHeight: 100
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[alert type="' + e.data.AlertType + '" dismiss="' + e.data.AlertDismiss + '"]' + e.data.AlertContent + '[/alert]');
							}
						});
					}
				},
				{
					text: pettyLang.shortcode_video,
					onclick: function() {
						editor.windowManager.open( {
							title: pettyLang.shortcode_video,
							body: [
								{
									type: 'textbox',
									name: 'VideoURL',
									label: pettyLang.shortcode_video_url,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'VideoWidth',
									label: pettyLang.shortcode_width,
									value: ''
								},
								{
									type: 'textbox',
									name: 'Videoheight',
									label: pettyLang.shortcode_height,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[embed width="' + e.data.VideoWidth + '" height="' + e.data.Videoheight + '"]' + e.data.VideoURL + '[/embed]');
							}
						});
					}
				},
				{
					text: pettyLang.shortcode_audio,
					onclick: function() {
						editor.windowManager.open( {
							title: pettyLang.shortcode_audio,
							body: [
								{
									type: 'textbox',
									name: 'mp3URL',
									label: pettyLang.shortcode_mp3,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'm4aURL',
									label: pettyLang.shortcode_m4a,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'oggURL',
									label: pettyLang.shortcode_ogg,
									value: 'http://',
									minWidth: 300,
								},
								
							],
							onsubmit: function( e ) {
								editor.insertContent( '[audio mp3="' + e.data.mp3URL + '" m4a="' + e.data.m4aURL + '" ogg="' + e.data.oggURL + '"]');
							}
						});
					}
				},
				{
					text: pettyLang.shortcode_tooltip,
					onclick: function() {
						editor.windowManager.open( {
							title: pettyLang.shortcode_tooltip,
							body: [
								{
									type: 'textbox',
									name: 'TooltipText',
									label: pettyLang.shortcode_text,
									value: '',
									minWidth: 300,
								},
								{
									type: 'listbox',
									name: 'TooltipDirection',
									label: pettyLang.shortcode_direction,
									'values': [
										{text: pettyLang.shortcode_top, value: 'top'},
										{text: pettyLang.shortcode_right, value: 'right'},
										{text: pettyLang.shortcode_bottom, value: 'bottom'},
										{text: pettyLang.shortcode_left, value: 'left'},
									]
								},
								{
									type: 'textbox',
									name: 'ToolTipContent',
									label: pettyLang.shortcode_content,
									value: '',
									multiline: true,
									minWidth: 300,
									minHeight: 100
								}
								
							],
							onsubmit: function( e ) {
								editor.insertContent( '[tooltip text="' + e.data.TooltipText + '" direction="' + e.data.TooltipDirection + '"]'+ e.data.ToolTipContent +'[/tooltip]');
							}
						});
					}
				},
				{
					text: pettyLang.shortcode_columns,
						onclick: function() {
							editor.insertContent( '\
							[row]<br />\
								[column number="6" offset=""] Column Content Goes Here [/column]<br />\
								[column number="6" offset=""] Column Content Goes Here [/column]<br />\
							[/row]');
						}
				},
				{
					text: pettyLang.shortcode_accordion,
						onclick: function() {
							editor.insertContent( '\
							[collapse_group]<br />\
								[collapse id="accordion_one" title="Accordion Title" expanded="false"] Accordion Content [/collapse]<br />\
								[collapse id="accordion_two" title="Accordion Title" expanded="false"] Accordion Content [/collapse]<br />\
							[/collapse_group]');
						}
				},
				{
					text: pettyLang.shortcode_tab,
						onclick: function() {
							editor.insertContent( '\
							[tabs]<br />\
								[tab id="tab_one" title="Tab Title" active="active"] Tab Content [/tab]<br />\
								[tab id="tab_two" title="Tab Title"] Tab Content [/tab]<br />\
								[tab id="tab_three" title="Tab Title"] Tab Content [/tab]<br />\
							[/tabs]');
						}
				},
				{
                    text: pettyLang.shortcode_custom_icon,
                    classes: 'sh-custom-icon',
                },
			]
		});
	});
})();