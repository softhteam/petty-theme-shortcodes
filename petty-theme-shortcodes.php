<?php 
/*
Plugin Name: Petty Theme Shortcodes
Plugin URI: http://www.softhopper.net
Description: This plugin will include Petty theme shortcode
Author: SoftHopper
Author URI: http://softhopper.net
Version: 1.0
*/

/* don't call the file directly */
if ( !defined( 'ABSPATH' ) ) exit;

// include shortcode file to generate shortcode from code editor
include( plugin_dir_path( __FILE__ ) . 'shortcodes/shortcodes.php' );